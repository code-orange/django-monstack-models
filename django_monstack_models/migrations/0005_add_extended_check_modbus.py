# Generated by Django 3.0.6 on 2020-06-02 19:54

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("django_monstack_check_commands", "0001_initial"),
        ("django_monstack_models", "0004_remove_fbc_models"),
    ]

    operations = [
        migrations.CreateModel(
            name="ExtendedCheckModbus",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("write_coil", models.IntegerField(blank=True, null=True)),
                ("write_coil_fail_val", models.IntegerField(blank=True, null=True)),
                (
                    "check_relation",
                    models.OneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        to="django_monstack_check_commands.CheckModbus",
                    ),
                ),
            ],
            options={
                "db_table": "extended_check_modbus",
            },
        ),
    ]
