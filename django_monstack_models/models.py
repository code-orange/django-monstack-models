from django_monstack_check_commands.django_monstack_check_commands.models import *


class CollectAppsData(models.Model):
    app_id = models.BigAutoField(primary_key=True)
    app_name = models.CharField(unique=True, max_length=250)

    class Meta:
        db_table = "collect_apps_data"


class CollectAppsRel(models.Model):
    apps_rel_id = models.BigAutoField(primary_key=True)
    app = models.ForeignKey(CollectAppsData, models.CASCADE)
    computer_id_ici = models.IntegerField(blank=False, null=False)

    class Meta:
        db_table = "collect_apps_rel"
        unique_together = (("computer_id_ici", "app"),)


class CollectServicesData(models.Model):
    service_id = models.BigAutoField(primary_key=True)
    service_name = models.CharField(unique=True, max_length=250)

    class Meta:
        db_table = "collect_services_data"


class CollectServicesRel(models.Model):
    services_rel_id = models.BigAutoField(primary_key=True)
    service = models.ForeignKey(CollectServicesData, models.CASCADE)
    computer_id_ici = models.IntegerField(blank=False, null=False)

    class Meta:
        db_table = "collect_services_rel"
        unique_together = (("computer_id_ici", "service"),)


class ExtendedCheckModbus(models.Model):
    check_relation = models.OneToOneField(CheckModbus, models.CASCADE)
    write_coil = models.IntegerField(null=True, blank=True)
    write_coil_fail_val = models.IntegerField(default=1)

    class Meta:
        db_table = "extended_check_modbus"
